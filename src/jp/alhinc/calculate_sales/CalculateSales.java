package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String ITEMSFILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String ITEMSFILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String SALES_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_INVALID_CODE = "の支店コードが不正です";
	private static final String ITEMS_INVALID_CODE = "の商品コードが不正です";
	private static final String SALES_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数の設定確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> itemsNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> itemsSales = new HashMap<>();


		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", FILE_NOT_EXIST, FILE_INVALID_FORMAT)) {
			return;
		}
		//商品定義ファイル読み込み
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, itemsNames, itemsSales, "^[A-Za-z0-9]{8}$",ITEMSFILE_NOT_EXIST, ITEMSFILE_INVALID_FORMAT)) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {

			//ファイル名表示
			String fileName = files[i].getName();
			if(fileName.matches("^[0-9]{8}+.rcd")) {
				rcdFiles.add(files[i]);
			}
		}
		//ファイルが連番になっているのを確認
		Collections.sort(rcdFiles);

		for (int i=0; i<rcdFiles.size() -1; i++ ) {

			int former= Integer.parseInt(rcdFiles.get(0).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}

		//2-2売り上げファイルパス
		BufferedReader br= null;
		for(int i = 0; i< rcdFiles.size(); i++){
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;

				//リストに格納
				List<String>lines = new ArrayList<>();

				while((line = br.readLine())  != null){
					// System.out.println(line);
					lines.add(line);
				}

				//ファイルのフォーマットが3桁以上を確認
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALES_INVALID_FORMAT);
					return;
				}

				//Mapに支店コードがKeyか確認
				if(!branchSales.containsKey(lines.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_INVALID_CODE);
					return;
				}

				//Mapに商品コードがKeyか確認
				if(!itemsSales.containsKey(lines.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + ITEMS_INVALID_CODE);
					return;
				}
				//数字を用いているか確認
				if(!lines.get(2).matches("[0-9]{1,10}")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//集計
				long fileSale = Long.parseLong(lines.get(2));

				Long itemsAmount =itemsSales.get(lines.get(1)) + fileSale;

				Long saleAmount = branchSales.get(lines.get(0))+fileSale;


				//売上金額１０桁以内を確認
				if(saleAmount>= 10000000000L || itemsAmount>= 10000000000L) {
					System.out.println(SALES_OVER);
					return;
				}


				branchSales.put(lines.get(0), saleAmount);
				itemsSales.put(lines.get(1), itemsAmount);

				br.close();
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品定義の追加書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, itemsNames, itemsSales)) {
			return;
		}
	}



	/**
	 * 支店定義ファイル読み込み処理
	 * @param fileInvalidFormat
	 * @param fileNotExist
	 * @param itemsfileInvalidFormat
	 * @param itemsfileNotExist
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap)
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String shopCode, String NotExist, String InvalidFormat ) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			if(!file.exists()) {
				System.out.println(NotExist);
				return false;
			}

			String line;
			String[] shop;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				shop = line.split(",");

				if((shop.length !=2) || !shop[0].matches(shopCode)) {
					System.out.println(InvalidFormat);
					return false;
				}
				Names.put(shop[0],shop[1]);
				Sales.put(shop[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			//for文で変数にkeyを代入、フォーマットに当てはまるよう書き込み
			for(String branchKey : branchSales.keySet()) {
				bw.write(branchKey + "," + branchNames.get(branchKey) + "," + branchSales.get(branchKey));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
